<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecepcionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recepciones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('marca', 100);
			$table->string('modelo', 100);
			$table->string('serial', 100)->nullable();
			$table->string('disco_duro', 50)->nullable();
			$table->integer('cant_modulos_memoria')->nullable();
			$table->integer('cant_gb_memoria')->nullable();
			$table->enum('bateria', array('Sí','No'))->default('No');
			$table->enum('cargador', array('Sí','No'))->default('No');
			$table->enum('bolso_forro', array('Sí','No'))->default('No');
			$table->enum('cable_poder', array('Sí','No'))->default('No');
			$table->enum('cable_vga', array('Sí','No'))->default('No');
			$table->enum('base_monitor', array('Sí','No'))->default('No');
			$table->text('otro_accesorio')->nullable();
			$table->text('caracteristicas')->nullable();
			$table->text('motivo_falla');
			$table->enum('estatus', array('Abierta','Lista para Entrega','Cerrada'))->default('Abierta');
			$table->enum('bandera', array('Perfecto estado','Con errores'))->default('Perfecto estado');
			$table->text('observacion')->nullable();
			$table->integer('cliente_id')->unsigned();
			$table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recepciones');
	}

}
