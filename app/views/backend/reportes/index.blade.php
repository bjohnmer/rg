@extends('backend.layout')

@section('content')
  <h3>Reportes</h3>
  <div class="row">
  <div class="col-sm-12">
    <a href="{{URL::to('/dashboard')}}" class="btn btn-warning" alt="Atras" title="Atras">
      <span class="glyphicon glyphicon-chevron-left"></span>&nbsp; Atras
    </a>
  </div>
  <div class="col-sm-12">
    <div class="col-sm-6 opcionesPP">
      <a class="btn btn-primary btn-lg center-block" href="{{ URL::to('/clients/create') }}">
        <small>
          <h3>Reporte de</h3>
            <h2><span class="glyphicon glyphicon-user"></span></h2>
            <strong><p>Clientes</p></strong>
        </small>
      </a>
    </div>
    <div class="col-sm-6 opcionesPP">
      <a class="btn btn-warning btn-lg center-block" href="{{ URL::to('/get/create') }}">
        <small>
          <h3>Reporte de</h3>
          <h2><span class="glyphicon glyphicon-log-in"></span></h2>
          <strong><p>Recepciones</p></strong>
        </small>
      </a>
    </div>
    <div class="col-sm-6 opcionesPP">
      <a class="btn btn-danger btn-lg center-block" href="{{ URL::to('/return') }}">
        <small>
          <h3>Reporte de</h3>
          <h2><span class="glyphicon glyphicon-log-out"></span></h2>
          <strong><p>Entregas</p></strong>
        </small>
      </a>
    </div>
    <div class="col-sm-6 opcionesPP">
      <a class="btn btn-success btn-lg center-block" href="{{ URL::to('/reports') }}">
        <small>
          <h3>Reporte de</h3>
          <h2><span class="glyphicon glyphicon-lock"></span></h2>
          <strong><p>Usuarios</p></strong>
        </small>
      </a>
    </div>
  </div>
@stop