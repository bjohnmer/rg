<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Aplicación Web para el Control del Área de Servicio Técnico de la Cooperativa RG Computación, R.L.">
    <meta name="author" content=" @aurimarbecerra|Aurimar Becerra y @bjohnmer|Johnmer Bencomo">
    <title>.: RG Computación, R.L. :.</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ URL::to('/') }}/css/bootstrap.min.css" media="screen">

    <!-- Custom styles -->
    <link rel="stylesheet" href="{{ URL::to('/') }}/css/personales.css" media="screen">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="container-fluid">
      <div class="col-sm-offset-2 col-sm-8">
        <nav role="navigation" class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a href="{{ URL::to('/') }}" class="navbar-brand">
                <img src="{{ URL::to('/') }}/img/logo.jpg" alt="RG Computación, R.L." class="img-responsive logo">
              </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="bs-example-navbar-collapse-1" class="navbar-collapse collapse" style="height: 1px;">
                           
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">Registros <span class="caret"></span></a>
                  <ul role="menu" class="dropdown-menu">
                    <li><a href="{{ URL::to('/clients') }}">Clientes</a></li>
                    <li><a href="{{ URL::to('/get') }}">Recepciones</a></li>
                    <li><a href="{{ URL::to('/return') }}">Entregas</a></li>
                    <!-- 
                    <li class="divider"></li>
                    -->
                  </ul>
                </li>
                <li><a href="{{ URL::to('/reports') }}">Reportes</a></li>
                <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">Configuraciones <span class="caret"></span></a>
                  <ul role="menu" class="dropdown-menu">
                    <li><a href="{{ URL::to('/users') }}">Usuarios</a></li>
                    <!-- <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li> -->
                  </ul>
                </li>
                <li><a href="{{ URL::to('/logout') }}" class="logout"><span class="glyphicon glyphicon-remove"></span> Cerrar</a></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
        
        <hr>
        
        @yield('content')
        
        <div class="col-sm-12 text-center">
          <hr>
          <!-- <footer> -->
            <p>Desarrollado por: <a href="http://twitter.com/aurimarbecerra" target="_blank">Aurimar Becerra</a> y <a href="http://twitter.com/bjohnmer" target="_blank">Johnmer Bencomo</a><br>Todos los Derechos Reservados <a href="#" target="_blank">Cooperativa RG Computación, R.L. &#0174;<?=date('Y')?></a>.</p>
          <!-- </footer> -->
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ URL::to('/') }}/js/jquery.js"></script>
    <script src="{{ URL::to('/') }}/js/bootstrap.min.js"></script>
    <script src="{{ URL::to('/') }}/js/custom.js"></script>

  </body>
</html>
