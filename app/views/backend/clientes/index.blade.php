@extends('backend.layout')

@section('content')
                  
  <h3>Clientes</h3>
  <div class="row">
    <div class="col-sm-12">
      <a href="{{URL::to('/clients/create')}}" class="btn btn-success" alt="Nuevo Cliente" title="Nuevo Cliente">
        <span class="glyphicon glyphicon-edit"></span>&nbsp; Nuevo Cliente 
      </a>
      <a href="{{URL::to('/dashboard')}}" class="btn btn-warning" alt="Atras" title="Atras">
        <span class="glyphicon glyphicon-chevron-left"></span>&nbsp; Atras 
      </a>
    </div>
    
    <div class="col-sm-12">
      @if(Session::has('message')) 
        <div class="alert alert-{{ Session::get('class') }} fade in">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
          <p>{{ Session::get('message') }}</p>
        </div>
      @endif

      @if($errors->has())               
        <div class="alert alert-danger fade in">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
          @foreach($errors->all() as $error)
              <p>{{ $error  }}</p>
          @endforeach
        </div>
      @endif

    </div>
    <div class="col-sm-12">
      @if (count($clientes)>0)
        <table class="table">
          <thead>
            <tr>
              <th width="10%">Cédula</th>
              <th width="25%">Nombre</th>
              <th width="25%">Email</th>
              <th width="10%">Teléfonos</th>
              <th class="text-right">Acciones</th>
            </tr>
          </thead>  
          <tbody>
        @foreach ($clientes as $c)
            <tr>
              <td>{{$c->cedula}}</td>
              <td>{{$c->nombre}}</td>
              <td>{{$c->email}}</td>
              <td>{{$c->telf}}</td>
              <td class="text-right">
                <a href="{{URL::to('/clients/'.$c->id.'/edit') }}" class="btn btn-primary" alt="Modificar" title="Modificar" id="modificar" >
                  <span class="glyphicon glyphicon-edit"></span>
                </a>
                {{ Form::open(array('url' => 'clients/' . $c->id, 'class' => 'pull-right')) }}
                  {{ Form::hidden('_method', 'DELETE') }}
                  <button type="submit" class="btn btn-danger eliminar" alt="Eliminar" title="Eliminar" >
                    <span class="glyphicon glyphicon-trash"></span>
                  </button>
                {{ Form::close() }}
              </td>
            </tr>
        @endforeach
          </tbody>
        </table>
      @else
        <div role="alert" class="alert alert-warning">
          <strong>NO</strong> Hay clientes registrados.
        </div>
      @endif
    </div>
  </div>

@stop