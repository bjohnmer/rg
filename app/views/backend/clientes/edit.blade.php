@extends('backend.layout')

@section('content')
                  
  <h3>Editar Cliente</h3>
  <div class="row">
    <div class="col-sm-12">
      <a href="{{URL::to('/clients')}}" class="btn btn-warning" alt="Atras" title="Atras">
        <span class="glyphicon glyphicon-chevron-left"></span>&nbsp; Atras 
      </a>
    </div>
  </div>
  
  <div class="col-sm-12">&nbsp;</div>
  <div class="col-sm-12">
    @if(Session::has('message'))
        <div class="alert alert-{{ Session::get('class') }} fade in">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <p>{{ Session::get('message') }}</p>
        </div>
    @endif
    @if($errors->has())               
         <div class="alert alert-danger fade in">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
         @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
         @endforeach
       </div>
    @endif
    <form role="form" class="form-horizontal" action="{{ URL::to('/clients/'.$cliente->id) }}" method="POST">
      {{ Form::hidden('_method', 'PUT') }}
      <div class="form-group">
          <label class="col-sm-4 control-label" for="cedula">Cédula</label>
          <div class="col-sm-8">
              <input type="text" name="cedula" id="cedula" value="{{ $cliente->cedula }}" class="form-control" placeholder="Ingrese la Cédula del Cliente" required>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 control-label" for="nombre">Nombre</label>
          <div class="col-sm-8">
              <input type="text" name="nombre" id="nombre" value="{{ $cliente->nombre }}" class="form-control" placeholder="Ingrese el Nombre del Cliente" required>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 control-label" for="email">Correo Electrónico</label>
          <div class="col-sm-8">
              <input type="email" name="email" id="email" value="{{ $cliente->email }}" class="form-control" placeholder="Ingrese el Email del Cliente" required>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 control-label" for="direccion">Dirección</label>
          <div class="col-sm-8">
            <textarea name="direccion" id="direccion"class="form-control" placeholder="Ingrese la Dirección" required>{{ $cliente->direccion }}</textarea>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 control-label" for="telf">Teléfono</label>
          <div class="col-sm-8">
              <input type="text" name="telf" id="telf" class="form-control" placeholder="Ingrese un Teléfono de contacto" value="{{ $cliente->telf }}" required>
          </div>
      </div>
      
      
      <div class="form-group">
          <div class="col-sm-offset-2 col-sm-8">
              <button class="btn btn-primary" type="submit">Modificar</button>
          </div>
      </div>

      
    </form>
  </div>

@stop