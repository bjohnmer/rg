@extends('backend.layout')

@section('content')
                  
  <h3>Usuarios</h3>
  <div class="row">
    <div class="col-sm-12">
      <a href="{{URL::to('/users/create')}}" class="btn btn-success" alt="Nuevo Usuario" title="Nuevo Usuario">
        <span class="glyphicon glyphicon-edit"></span>&nbsp; Nuevo Usuario 
      </a>
      <a href="{{URL::to('/dashboard')}}" class="btn btn-warning" alt="Atras" title="Atras">
        <span class="glyphicon glyphicon-chevron-left"></span>&nbsp; Atras 
      </a>
    </div>
    
    <div class="col-sm-12">
      @if(Session::has('message')) 
        <div class="alert alert-{{ Session::get('class') }} fade in">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
          <p>{{ Session::get('message') }}</p>
        </div>
      @endif

      @if($errors->has())               
        <div class="alert alert-danger fade in">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
          @foreach($errors->all() as $error)
              <p>{{ $error  }}</p>
          @endforeach
        </div>
      @endif

    </div>
    <div class="col-sm-12">
      @if (count($usuarios)>0)
        <table class="table">
          <thead>
            <tr>
              <th width="25%">Nombre</th>
              <th width="25%">Login</th>
              <th width="10%">Rol</th>
              <th width="10%">Estatus</th>
              <th class="text-right">Acciones</th>
            </tr>
          </thead>  
          <tbody>
        @foreach ($usuarios as $u)
            <tr>
              <td>{{$u->nombre}}</td>
              <td>{{$u->login}}</td>
              <td>{{$u->rol}}</td>
              <td>{{$u->estatus}}</td>
              <td class="text-right">
                <a href="{{URL::to('/users/'.$u->id.'/edit') }}" class="btn btn-primary" alt="Modificar" title="Modificar" id="modificar" >
                  <span class="glyphicon glyphicon-edit"></span>
                </a>
                {{ Form::open(array('url' => 'users/' . $u->id, 'class' => 'pull-right')) }}
                  {{ Form::hidden('_method', 'DELETE') }}
                  <button type="submit" class="btn btn-danger eliminar" alt="Eliminar" title="Eliminar" >
                    <span class="glyphicon glyphicon-trash"></span>
                  </button>
                {{ Form::close() }}
              </td>
            </tr>
        @endforeach
          </tbody>
        </table>
      @else
        <div role="alert" class="alert alert-warning">
          <strong>NO</strong> Hay usuarios registrados.
        </div>
      @endif
    </div>
  </div>

@stop