@extends('backend.layout')

@section('content')
                  
  <h3>Nuevo Usuario</h3>
  <div class="row">
    <div class="col-sm-12">
      <a href="{{URL::to('/users')}}" class="btn btn-warning" alt="Atras" title="Atras">
        <span class="glyphicon glyphicon-chevron-left"></span>&nbsp; Atras 
      </a>
    </div>
  </div>
  
  <div class="col-sm-12">&nbsp;</div>
  <div class="col-sm-12">
    @if(Session::has('message'))
        <div class="alert alert-{{ Session::get('class') }} fade in">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <p>{{ Session::get('message') }}</p>
        </div>
    @endif
    @if($errors->has())               
         <div class="alert alert-danger fade in">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
         @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
         @endforeach
       </div>
    @endif
    <form role="form" class="form-horizontal" action="{{ URL::to('/users') }}" method="post">
      <div class="form-group">
          <label class="col-sm-4 control-label" for="nombre">Nombre</label>
          <div class="col-sm-8">
              <input type="text" name="nombre" id="nombre" value="{{ Input::old('nombre') }}" class="form-control" placeholder="Ingrese el Nombre del Usuario" required>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 control-label" for="email">Correo Electrónico</label>
          <div class="col-sm-8">
              <input type="email" name="email" id="email" value="{{ Input::old('email') }}" class="form-control" placeholder="Ingrese el Email del usuario" required>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 control-label" for="rol">Rol</label>
          <div class="col-sm-8">
            <select name="rol" id="rol" class="form-control" required>
              <option value="Usuario">Usuario</option>
              <option value="Administrador">Administrador</option>
            </select>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 control-label" for="clave">Contraseña</label>
          <div class="col-sm-8">
              <input type="password" name="clave" id="clave" class="form-control" placeholder="Ingrese una Clave" required>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 control-label" for="cclave">Confirmar Contraseña</label>
          <div class="col-sm-8">
              <input type="password" name="cclave" id="cclave" class="form-control"  placeholder="Confirme la Clave" required>
          </div>
      </div>
      
      <div class="form-group">
          <div class="col-sm-offset-2 col-sm-8">
              <button class="btn btn-primary" type="submit">Guardar</button>
          </div>
      </div>

      
    </form>
  </div>

@stop