@extends('backend.layout')

@section('content')
                  
  <h3>Editar Recepción</h3>
  <div class="row">
    <div class="col-sm-12">
      <a href="{{URL::to('/get')}}" class="btn btn-warning" alt="Atras" title="Atras">
        <span class="glyphicon glyphicon-chevron-left"></span>&nbsp; Atras 
      </a>
    </div>
  </div>
  
  <div class="col-sm-12">&nbsp;</div>
  <div class="col-sm-12">
    @if(Session::has('message'))
        <div class="alert alert-{{ Session::get('class') }} fade in">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <p>{{ Session::get('message') }}</p>
        </div>
    @endif
    @if($errors->has())               
         <div class="alert alert-danger fade in">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
         @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
         @endforeach
       </div>
    @endif

    <form role="form" class="form-horizontal" action="{{ URL::to('/clients') }}" method="post">
      
      <div class="form-group">
          <label class="col-sm-4 control-label" for="cliente">Cliente</label>
          <div class="col-sm-8">
              <select name="cliente" id="cliente">
                @if (count($clientes)>0)
                  @foreach ($clientes as $c)
                <option value="{{$c->id}}">{{$c->cedula}} {{$c->nombre}}</option>
                  @endforeach
                @else
                  <option value="">No hay clientes registrados</option>
                @endif
              </select>
          </div>
      </div>

      <div class="col-sm-12">
        <ul role="tablist" class="nav nav-tabs" id="myTab">
          <li class="active"><a data-toggle="tab" role="tab" href="#equipo">Datos del Equipo</a></li>
          <li class=""><a data-toggle="tab" role="tab" href="#accesorios">Accesorios</a></li>
          <li class=""><a data-toggle="tab" role="tab" href="#carac">Características y Observaciones</a></li>
          
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="col-sm-12">&nbsp;</div>
            <div id="equipo" class="tab-pane fade active in">
              <div class="form-group">
                  <label class="col-sm-4 control-label" for="marca">Marca</label>
                  <div class="col-sm-8">
                      <input type="text" name="marca" id="marca" value="{{ Input::old('marca') }}" class="form-control" placeholder="Ingrese el marca del Equipo" required>
                  </div>
              </div>
            
              <div class="form-group">
                  <label class="col-sm-4 control-label" for="modelo">Modelo</label>
                  <div class="col-sm-8">
                      <input type="text" name="modelo" id="modelo" value="{{ Input::old('modelo') }}" class="form-control" placeholder="Ingrese el modelo del Equipo" required>
                  </div>
              </div>
            
              <div class="form-group">
                  <label class="col-sm-4 control-label" for="serial">Serial</label>
                  <div class="col-sm-8">
                      <input type="text" name="serial" id="serial" value="{{ Input::old('serial') }}" class="form-control" placeholder="Ingrese el serial del Equipo">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-4 control-label" for="disco_duro">Disco Duro</label>
                  <div class="col-sm-8">
                      <input type="text" name="disco_duro" id="disco_duro" value="{{ Input::old('disco_duro') }}" class="form-control" placeholder="Ingrese las caracteristicas del Disco Duro del Equipo">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-4 control-label" for="cant_modulos_memoria">Cantidad de Módulos de Memoria</label>
                  <div class="col-sm-8">
                      <input type="number" min="1" max="8" name="cant_modulos_memoria" id="cant_modulos_memoria" value="{{ Input::old('cant_modulos_memoria') }}" class="form-control" placeholder="Ingrese la Cantidad de Módulos de Memoria">
                  </div>
              </div>
              <div class="form-group">
                  <label class="col-sm-4 control-label" for="cant_gb_memoria">Cantidad de Memoria en Gb</label>
                  <div class="col-sm-8">
                      <input type="number" name="cant_gb_memoria" id="cant_gb_memoria" value="{{ Input::old('cant_gb_memoria') }}" class="form-control" placeholder="Ingrese la Cantidad de Memoria en Gb">
                  </div>
              </div>
          </div>
              
          <div id="accesorios" class="tab-pane fade">
              <div class="col-sm-12">&nbsp;</div>
            <div class="form-group">
                <label class="col-sm-6 control-label" for="bateria">¿Deja Batería?</label>
                <div class="col-sm-2 checkbox">
                    <input type="checkbox" name="bateria" id="bateria" value="Sí">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-6 control-label" for="cargador">¿Deja Cargador?</label>
                <div class="col-sm-2 checkbox">
                    <input type="checkbox" name="cargador" id="cargador" value="Sí">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-6 control-label" for="bolso_forro">¿Deja Bolso o Forro?</label>
                <div class="col-sm-2 checkbox">
                    <input type="checkbox" name="bolso_forro" id="bolso_forro" value="Sí">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-6 control-label" for="cable_poder">¿Deja Cable de Poder?</label>
                <div class="col-sm-2 checkbox">
                    <input type="checkbox" name="cable_poder" id="cable_poder" value="Sí">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-6 control-label" for="cable_vga">¿Deja Cable VGA, DVI o Super Video?</label>
                <div class="col-sm-2 checkbox">
                    <input type="checkbox" name="cable_vga" id="cable_vga" value="Sí">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-6 control-label" for="base_monitor">¿Deja Base de Monitor?</label>
                <div class="col-sm-2 checkbox">
                    <input type="checkbox" name="base_monitor" id="base_monitor" value="Sí">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="otro_accesorio">¿Deja algún otro accesorio? (Describa)</label>
                <div class="col-sm-8">
                    <input type="text" name="otro_accesorio" id="otro_accesorio" value="{{ Input::old('otro_accesorio') }}" class="form-control" placeholder="Describa otro accesorio">
                </div>
            </div>
            
          </div>
          <div id="carac" class="tab-pane fade">
              <div class="col-sm-12">&nbsp;</div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="caracteristicas">Características del Equipo</label>
                <div class="col-sm-8">
                  <textarea name="caracteristicas" id="caracteristicas"class="form-control" placeholder="Ingrese otras Características del Equipo" required>{{ Input::old('caracteristicas') }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="motivo_falla">Motivo o Falla</label>
                <div class="col-sm-8">
                  <textarea name="motivo_falla" id="motivo_falla"class="form-control" placeholder="Ingrese el Motivo o la Falla" required>{{ Input::old('motivo_falla') }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="observacion">Observación</label>
                <div class="col-sm-8">
                  <textarea name="observacion" id="observacion"class="form-control" placeholder="Ingrese el Motivo o la Falla" required>{{ Input::old('observacion') }}</textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label" for="estatus">Estatus</label>
                <div class="col-sm-8">
                    <select name="estatus" id="estatus">
                      <option value="Abierta">Abierta</option>
                      <option value="Lista para Entrega">Lista para Entrega</option>
                      <option value="Cerrada">Cerrada</option>
                    </select>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-4 control-label" for="bandera">Dato Especial</label>
                <div class="col-sm-8">
                    <select name="bandera" id="bandera">
                      <option value="Perfecto estado">Perfecto estado</option>
                      <option value="Con errores">Con errores</option>
                    </select>
                </div>
            </div>
          </div>
          
        </div>
      </div>
      
      <div class="form-group">
          <div class="col-sm-offset-2 col-sm-8">
              <button class="btn btn-primary" type="submit">
                <span class="glyphicon glyphicon-edit"></span>
                &nbsp;Actualizar
              </button>
          </div>
      </div>

      
    </form>
  </div>

@stop