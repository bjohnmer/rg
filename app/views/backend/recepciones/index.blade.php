@extends('backend.layout')

@section('content')
                  
  <h3>Recepciones</h3>
  <div class="row">
    <div class="col-sm-12">
      <a href="{{URL::to('/get/create')}}" class="btn btn-success" alt="Nueva Recepción de Equipo" title="Nueva Recepción de Equipo">
        <span class="glyphicon glyphicon-edit"></span>&nbsp; Nueva Recepción de Equipo
      </a>
      <a href="{{URL::to('/dashboard')}}" class="btn btn-warning" alt="Atras" title="Atras">
        <span class="glyphicon glyphicon-chevron-left"></span>&nbsp; Atras
      </a>
    </div>
    
    <div class="col-sm-12">
      @if(Session::has('message')) 
        <div class="alert alert-{{ Session::get('class') }} fade in">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
          <p>{{ Session::get('message') }}</p>
        </div>
      @endif

      @if($errors->has())               
        <div class="alert alert-danger fade in">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
          @foreach($errors->all() as $error)
              <p>{{ $error  }}</p>
          @endforeach
        </div>
      @endif

    </div>
    <div class="col-sm-12">
      @if (count($recepciones)>0)
        <table class="table">
          <thead>
            <tr>
              <th width="5%">#</th>
              <th width="30%">Cliente</th>
              <th width="10%">Marca</th>
              <th width="15%">Modelo</th>
              <th width="20%">Motivo/Falla</th>
              <th class="text-right">Acciones</th>
            </tr>
          </thead>  
          <tbody>
        @foreach ($recepciones as $r)
            <tr>
              <td>{{ $r->id }}</td>
              <td>{{ $r->cliente->cedula }} {{ $r->cliente->nombre }}</td>
              <td>{{ $r->marca }}</td>
              <td>{{ $r->modelo }}</td>
              <td>{{ $r->motivo_falla}}</td>
              <td class="text-right">
                <a href="{{URL::to('/get/'.$r->id.'/edit') }}" class="btn btn-primary" alt="Modificar" title="Modificar" id="modificar" >
                  <span class="glyphicon glyphicon-edit"></span>
                </a>
                {{ Form::open(array('url' => 'get/' . $r->id, 'class' => 'pull-right')) }}
                  {{ Form::hidden('_method', 'DELETE') }}
                  <button type="submit" class="btn btn-danger eliminar" alt="Eliminar" title="Eliminar" >
                    <span class="glyphicon glyphicon-trash"></span>
                  </button>
                {{ Form::close() }}
              </td>
            </tr>
        @endforeach
          </tbody>
        </table>
      @else
        <div role="alert" class="alert alert-warning">
          <strong>NO</strong> Hay Recepciones registradas.
        </div>
      @endif
    </div>
  </div>

@stop