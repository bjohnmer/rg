@extends('backend.layout')

@section('content')
                  
        <div class="col-sm-12">
          <div class="col-sm-6 opcionesPP">
            <a class="btn btn-primary btn-lg center-block" href="{{ URL::to('/clients/create') }}">
              <small>
                  <h3>Paso 1</h3>
                  <h2><span class="glyphicon glyphicon-user"></span></h2>
                  <strong><p>Registrar Cliente</p></strong>
              </small>
            </a>
          </div>
          <div class="col-sm-6 opcionesPP">
            <a class="btn btn-warning btn-lg center-block" href="{{ URL::to('/get/create') }}">
              <small>
                <h3>Paso 2</h3>
                <h2><span class="glyphicon glyphicon-log-in"></span></h2>
                <strong><p>Recibir Equipo</p></strong>
              </small>
            </a>
          </div>
        <!-- </div>
        <div class="col-sm-12"> -->
          <div class="col-sm-6 opcionesPP">
            <a class="btn btn-danger btn-lg center-block" href="{{ URL::to('/return') }}">
              <small>
                <h3>Paso 3</h3>
                <h2><span class="glyphicon glyphicon-log-out"></span></h2>
                <strong><p>Entregar Equipo</p></strong>
              </small>
            </a>
          </div>
          <div class="col-sm-6 opcionesPP">
            <a class="btn btn-success btn-lg center-block" href="{{ URL::to('/reports') }}">
              <small>
                <h3>Paso 4</h3>
                <h2><span class="glyphicon glyphicon-print"></span></h2>
                <strong><p>Ver Reportes</p></strong>
              </small>
            </a>
          </div>
        </div>
@stop