@extends('frontend.layout')

@section('content')

<div class="col-sm-12">
  <div class="col-sm-12">
    <img id="logo" src="{{ URL::to('/') }}/img/logo.jpg" class="center-block img-responsive" alt="RG Computación, R.L." title="RG Computación, R.L.">
  </div>
  <div class="col-sm-12 text-center">
    <h3 class="cover-heading">
      Aplicación Web para el Control del Área de Servicio Técnico de la Cooperativa RG Computación, R.L.
    </h3>
  </div>
  
  <div class="col-sm-12 text-center">
    &nbsp;
  </div>
  <div class="col-sm-12 text-center">
    <h4>Ingresar a la WebApp</h4>
  </div>
  <div class="col-sm-12">
    @if(Session::has('message'))
    <div class="alert alert-{{ Session::get('class') }} fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
        <p>{{ Session::get('message') }}</p>
    </div>
    @endif
    @if($errors->has())               
    <div class="alert alert-danger fade in">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
      @foreach($errors->all() as $error)
       <p>{{ $error  }}</p>
      @endforeach
    </div>
    @endif
  </div>
  <div class="col-sm-offset-4 col-sm-4 text-center">
    <form role="form" class="form-horizontal" action="{{ URL::to('/login') }}" method="post">
      <div class="form-group">
        <input type="email" placeholder="Escriba su email" id="email" name="email" class="form-control text-center" tabindex="1">
      </div>
      <div class="form-group">
        <input type="password" placeholder="Escriba una Clave" id="clave" name="clave" class="form-control text-center" tabindex="2">
      </div>
      <div class="form-group">
        <button class="btn btn-danger" type="submit">Entrar</button>
        <a href="{{ URL::to('/newreg') }}" class="btn btn-link">Registrese</a>
      </div>
    </form>
  </div>
</div>
@stop