<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Aplicación Web para el Control del Área de Servicio Técnico de la Cooperativa RG Computación, R.L.">
    <meta name="author" content=" @aurimarbecerra|Aurimar Becerra y @bjohnmer|Johnmer Bencomo">
    <title>.: RG Computación, R.L. :.</title>

    <!-- Bootstrap core CSS -->
    {{ HTML::style ('css/bootstrap.min.css', array ('media'=>'screen')) }}

    <!-- Custom styles -->
    {{ HTML::style ('css/personales.css', array ('media'=>'screen')) }}
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="container">
      <div class="col-sm-offset-2 col-sm-8">
        <nav role="navigation" class="navbar">
          <!-- We use the fluid option here to avoid overriding the fixed width of a normal container within the narrow content columns. -->
          <div class="container-fluid">
            <div class="navbar-header">
              <button data-target="#bs-example-navbar-collapse-6" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a href="#" class="navbar-brand">RG Computación, R.L.</a>
            </div>

            <div id="bs-example-navbar-collapse-6" class="collapse navbar-collapse">
              <!-- <ul class="nav navbar-nav navbar-right">
                <li class="active"><a class="text-success" href="#">#</a></li>
              </ul> -->
            </div><!-- /.navbar-collapse -->
          </div>
        </nav>
        <hr>
        
        @yield('content')
        
        <div class="col-sm-12 text-center">
          <hr>
          <!-- <footer> -->
            <p>Desarrollado por: <a href="http://twitter.com/aurimarbecerra" target="_blank">Aurimar Becerra</a> y <a href="http://twitter.com/bjohnmer" target="_blank">Johnmer Bencomo</a><br>Todos los Derechos Reservados <a href="#" target="_blank">Cooperativa RG Computación, R.L. &#0174;<?=date('Y')?></a>.</p>
          <!-- </footer> -->
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ URL::to('/') }}/js/jquery.js"></script>
    <script src="{{ URL::to('/') }}/js/bootstrap.min.js"></script>

  </body>
</html>
