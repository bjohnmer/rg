@extends('frontend.layout')


@section('content')
<div class="col-sm-12">
  <div class="col-sm-12">
    <img id="logo" src="{{ URL::to('/') }}/img/logo.jpg" class="center-block img-responsive" alt="RG Computación, R.L." title="RG Computación, R.L.">
  </div>
  <div class="col-sm-12 text-center">
    <h3 class="cover-heading">
      Aplicación Web para el Control del Área de Servicio Técnico de la Cooperativa RG Computación, R.L.
    </h3>
    <h4>Registrar Usuario</h4>
    @if(Session::has('message'))
      <div class="alert alert-{{ Session::get('class') }} fade in">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
          <p>{{ Session::get('message') }}</p>
      </div>
    @endif
    @if($errors->has())               
     <div class="alert alert-danger fade in">
     <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
        @foreach($errors->all() as $error)
         <p>{{ $error }}</p>
        @endforeach
     </div>
    @endif
  </div>
  <div class="col-sm-12 text-center">
    <form role="form" class="form-horizontal" action="{{ URL::to('/save') }}" method="post">
        <div class="form-group">
            <label class="col-sm-4 control-label" for="nombre">Nombre</label>
            <div class="col-sm-8">
                <input type="text" name="nombre" id="nombre" value="{{ Input::old('nombre') }}" class="form-control" placeholder="Ingrese su Nombre" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="email">Correo Electrónico</label>
            <div class="col-sm-8">
                <input type="email" name="email" id="email" value="{{ Input::old('email') }}" class="form-control" placeholder="Ingrese su email" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="clave">Contraseña</label>
            <div class="col-sm-8">
                <input type="password" name="clave" id="clave" class="form-control" placeholder="Ingrese su clave" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="cclave">Confirmar Contraseña</label>
            <div class="col-sm-8">
                <input type="password" name="cclave" id="cclave" class="form-control"  placeholder="Confirme su clave" required>
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <a href="{{ URL::to('/') }}" class="btn btn-danger"><span class="glyphicon glyphicon-chevron-left"></span> Atrás</a>
                <button class="btn btn-primary" type="submit">Guardar</button>
            </div>
        </div>
    </form>
  </div>
</div>

@stop