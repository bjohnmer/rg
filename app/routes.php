<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group(array('before'=>'iflogged'),function ()
{
  /*
  |
  | Rutas de Frontend
  |
  */
  Route::get('/', 'HomeController@index');
  Route::get('/newreg', 'HomeController@newreg');
  Route::post('/save', 'HomeController@save');
  Route::post('/login', 'HomeController@login');
});

Route::group(array('before'=>'ifauth'),function() 
{
  /*
  |
  | Rutas permitidas a todos los usuarios
  |
  */
  Route::get('/dashboard', 'DashboardController@index');
  Route::get('/return', 'EntregasController@index');
  Route::get('/reports', 'ReportesController@index');
  Route::get('/logout', 'HomeController@logout');
  Route::resource('clients', 'ClientesController', ['except' => 'show']);
  Route::resource('get', 'RecepcionesController', ['except' => 'show']);

  Route::group(array('before'=>'ifadmin'),function() 
  {
    /*
    |
    | Rutas exclusivas de los usuarios con el rol Administrador
    |
    */
    Route::resource('users', 'UsuariosController', ['except' => 'show']);

  });

    
});