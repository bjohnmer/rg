<?php

class Usuario extends \Eloquent {
	
  protected $table = 'usuarios';
  protected $fillable =['nombre','login','email','estatus','rol','clave'];
  protected $guarded = ['id'];

  public static $reglasinsertar = [
    'nombre' => 'required|min:5|max:100',
    'email'  => 'required|email|unique:usuarios,email|min:6|max:40',
    'clave'  => 'required|same:cclave|min:6|max:25',
    'cclave'  => 'required|same:clave|min:6|max:25',
  ];

  public static $mensajesinsertar = [
    'email.required' => 'El campo <strong>Correo Electrónico</strong> es requerido',
    'email.email' => 'El formato del campo <strong>Correo Electrónico</strong> no es valido',
    'email.unique' => 'El <strong>Correo Electrónico</strong> ya ha sido registrado',
    'email.min' => 'El campo <strong>Correo Electrónico</strong> debe contener minimo :min carecteres',
    'email.max' => 'El campo <strong>Correo Electrónico</strong> debe contener maximo :max carecteres',
    'clave.required' => 'La <strong>Contraseña </strong> es requerida',
    'clave.same' => 'El campo <strong>Contraseña </strong> y el campo <strong>Confirmar Contraseña </strong> deben ser iguales ', 
    'clave.min' => 'El campo <strong>Contraseña </strong> debe contener minimo :min caracteres ', 
    'clave.max' => 'El campo <strong>Contraseña </strong> debe contener un maximo de :max caracteres ', 
    'cclave.required' => 'La <strong>Confirmar Contraseña </strong> es requerida',
    'cclave.same' => 'El campo <strong>Confirmar Contraseña </strong> y el campo <strong>Contraseña </strong> deben ser iguales ', 
    'cclave.min' => 'El campo <strong>Confirmar Contraseña </strong> debe contener minimo :min caracteres ', 
    'cclave.max' => 'El campo <strong>Confirmar Contraseña </strong> debe contener un maximo de :max caracteres '
  ];

  public static $reglascreate = [
    'nombre' => 'required|min:5|max:100',
    'email'  => 'required|email|unique:usuarios,email|min:6|max:40',
    'clave'  => 'required|same:cclave|min:6|max:25',
    'cclave'  => 'required|same:clave|min:6|max:25',
    'rol'  => 'required'
  ];

  public static $mensajescreate = [
    'email.required' => 'El campo <strong>Correo Electrónico</strong> es requerido',
    'email.email' => 'El formato del campo <strong>Correo Electrónico</strong> no es valido',
    'email.unique' => 'El <strong>Correo Electrónico</strong> ya ha sido registrado',
    'email.min' => 'El campo <strong>Correo Electrónico</strong> debe contener minimo :min carecteres',
    'email.max' => 'El campo <strong>Correo Electrónico</strong> debe contener maximo :max carecteres',
    'clave.required' => 'La <strong>Contraseña </strong> es requerida',
    'clave.same' => 'El campo <strong>Contraseña </strong> y el campo <strong>Confirmar Contraseña </strong> deben ser iguales ', 
    'clave.min' => 'El campo <strong>Contraseña </strong> debe contener minimo :min caracteres ', 
    'clave.max' => 'El campo <strong>Contraseña </strong> debe contener un maximo de :max caracteres ', 
    'cclave.required' => 'La <strong>Confirmar Contraseña </strong> es requerida',
    'cclave.same' => 'El campo <strong>Confirmar Contraseña </strong> y el campo <strong>Contraseña </strong> deben ser iguales ', 
    'cclave.min' => 'El campo <strong>Confirmar Contraseña </strong> debe contener minimo :min caracteres ', 
    'cclave.max' => 'El campo <strong>Confirmar Contraseña </strong> debe contener un maximo de :max caracteres ',
    'rol.required' => 'El <strong>Rol</strong> es requerido'
  ];

  public static $reglaslogin=[
      'email'  => 'required|min:6|max:40',
      'clave'  => 'required|min:6|max:25',
  ];
  public static $mensajeslogin = [
    'email.required' => 'El campo <strong>Correo Electrónico</strong> es requerido',
    'email.email' => 'El formato del campo <strong>Correo Electrónico</strong> no es valido',
    'email.min' => 'El campo <strong>Correo Electrónico</strong> debe contener minimo :min carecteres',
    'email.max' => 'El campo <strong>Correo Electrónico</strong> debe contener maximo :max carecteres',
    'clave.required' => 'La <strong>Contraseña </strong> es requerida',
    'clave.min' => 'El campo <strong>Contraseña </strong> debe contener minimo :min caracteres ', 
    'clave.max' => 'El campo <strong>Contraseña </strong> debe contener un maximo de :max caracteres ', 
  ];
  public static $reglasrenew=[
    'email'  => 'required|min:6|max:40',
  ];
  public static $mensajesrenew = [
    'email.required' => 'El campo <strong>Correo Electrónico</strong> es requerido',
    'email.email' => 'El formato del campo <strong>Correo Electrónico</strong> no es valido',
    'email.min' => 'El campo <strong>Correo Electrónico</strong> debe contener minimo :min carecteres',
    'email.max' => 'El campo <strong>Correo Electrónico</strong> debe contener maximo :max carecteres',
  ];
  public static $rulenewpass=[
    'aclave'  => 'required|min:6|max:25',
    'nclave'  => 'required|same:cclave|min:6|max:25',
    'cclave'  => 'required|same:nclave|min:6|max:25',
  ];
  public static $msjnewpass = [
    'aclave.required' => 'La <strong>Actual Contraseña </strong> es requerida',
    'aclave.min' => 'El campo <strong>Actual Contraseña </strong> debe contener minimo :min caracteres ', 
    'aclave.max' => 'El campo <strong>Actual Contraseña </strong> debe contener un maximo de :max caracteres ', 

    'nclave.required' => 'La <strong>Nueva Contraseña </strong> es requerida',
    'nclave.min' => 'La <strong>Nueva Contraseña </strong> debe contener minimo :min caracteres ', 
    'nclave.max' => 'La <strong>Nueva Contraseña </strong> debe contener un maximo de :max caracteres ', 
    'nclave.same' => 'La <strong>Nueva Contraseña </strong> y el campo <strong>Confirmar Contraseña </strong> deben ser iguales ', 

    'cclave.required' => 'La <strong>Confirmar Contraseña </strong> es requerida',
    'cclave.same' => 'El campo <strong>Confirmar Contraseña </strong> y el campo <strong>Nueva Contraseña </strong> deben ser iguales ', 
    'cclave.min' => 'El campo <strong>Confirmar Contraseña </strong> debe contener minimo :min caracteres ', 
    'cclave.max' => 'El campo <strong>Confirmar Contraseña </strong> debe contener un maximo de :max caracteres '
  ];

  public static function validacionesinsertar($data)
  {
    return Validator::make($data, static::$reglasinsertar, static::$mensajesinsertar);
  }
  public static function validacionescreate($data)
  {
    return Validator::make($data, static::$reglascreate, static::$mensajescreate);
  }
  
  public static function validacioneslogin($data)
  {
    return Validator::make($data, static::$reglaslogin, static::$mensajeslogin);
  }

  public static function validacionesrenew($data)
  {
    return Validator::make($data, static::$reglasrenew, static::$mensajesrenew);
  }

  public static function validacionesnewpass($data)
  {
    return Validator::make($data, static::$rulenewpass, static::$msjnewpass);
  }

}