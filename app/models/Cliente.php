<?php

class Cliente extends \Eloquent {
	protected $table = 'clientes';
  protected $fillable =['cedula','nombre','email','telf','direccion'];
  protected $guarded = ['id'];

  public static $rules = [
    'cedula' => 'required|integer',
    'nombre' => 'required|min:5|max:100',
    'email'  => 'email|unique:clientes,email|min:6|max:100',
    'telf'  => 'required|integer|digits_between:5,50',
    'direccion'  => 'required'
  ];
  public static $rulesU = [
    'cedula' => 'required|integer',
    'nombre' => 'required|min:5|max:100',
    'email'  => 'email|min:6|max:100',
    'telf'  => 'required|integer|digits_between:5,50',
    'direccion'  => 'required'
  ];

  public static function validaciones($data)
  {
    return Validator::make($data, static::$rules);
  }
  public static function validacionesU($data)
  {
    return Validator::make($data, static::$rulesU);
  }

  public function recepciones()
  {
    return $this->HasMany('Recepcion');
  }
 
}