<?php

class Recepcion extends \Eloquent {
  
  protected $table = 'recepciones';
  protected $fillable =['marca','modelo','serial','disco_duro','cant_modulos_memoria','cant_gb_memoria','bateria','cargador','bolso_forro','cable_poder','cable_vga','base_monitor','otro_accesorio','caracteristicas','motivo_falla','estatus','bandera','observacion','cliente_id'];
  protected $guarded = ['id'];

  public function cliente()
  {
    return $this->belongsTo('Cliente');
  }

}