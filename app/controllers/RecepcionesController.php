<?php

class RecepcionesController extends \BaseController {

	/**
	 * Display a listing of recepciones
	 *
	 * @return Response
	 */
	public function index()
	{
		$recepciones = Recepcion::has('cliente')->get();
		
		return View::make('backend.recepciones.index', compact('recepciones'));
	}

	/**
	 * Show the form for creating a new recepcion
	 *
	 * @return Response
	 */
	public function create()
	{
		$clientes = Cliente::all();

		return View::make('backend.recepciones.create', compact('clientes'));
	}

	/**
	 * Store a newly created recepcion in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Recepcion::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Recepcion::create($data);

		return Redirect::route('get.index');
	}

	/**
	 * Display the specified recepcion.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$recepcion = Recepcion::findOrFail($id);

		return View::make('backend.recepciones.show', compact('recepcion'));
	}

	/**
	 * Show the form for editing the specified recepcion.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$recepcion = Recepcion::find($id);

		return View::make('backend.recepciones.edit', compact('recepcion'));
	}

	/**
	 * Update the specified recepcion in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$recepcion = Recepcion::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Recepcion::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$recepcion->update($data);

		return Redirect::route('get.index');
	}

	/**
	 * Remove the specified recepcion from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Recepcion::destroy($id);

		return Redirect::route('get.index');
	}

}
