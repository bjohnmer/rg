<?php

class ClientesController extends \BaseController {

	/**
	 * Display a listing of cliente
	 *
	 * @return Response
	 */
	public function index()
	{
		$clientes = Cliente::all();

		return View::make('backend.clientes.index', compact('clientes'));
	}

	/**
	 * Show the form for creating a new usuario
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.clientes.create');
	}

	/**
	 * Store a newly created usuario in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$cliente=new Cliente;
		
		$validaciones = Cliente::validaciones(Input::all());

		if ($validaciones->fails())
		{
			return Redirect::back()->withErrors($validaciones)->withInput();
		}
		else
		{
					
			$cliente->cedula=Input::get('cedula');
			$cliente->nombre=Input::get('nombre');
			$cliente->email=Input::get('email');
			$cliente->direccion=Input::get('direccion');
			$cliente->telf=Input::get('telf');

			if($cliente->save())
			{
				Session::flash('message','Guardado correctamente!');
				Session::flash('class','success');
			}
			else
			{
				Session::flash('message','Ha ocurrido un error');
				Session::flash('class','danger');
			}
		}

		return Redirect::route('clients.index');
		
	}

	/**
	 * Display the specified usuario.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$cliente = Cliente::findOrFail($id);

		return View::make('backend.clientes.show', compact('usuario'));
	}

	/**
	 * Show the form for editing the specified usuario.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$cliente = Cliente::find($id);

		return View::make('backend.clientes.edit', compact('cliente'));
	}

	/**
	 * Update the specified usuario in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$cliente = Cliente::findOrFail($id);

		$validator = Cliente::validacionesU(Input::all());

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		else
		{
		
			if($cliente->update(Input::all()))
			{
				Session::flash('message','Actualizado Correctamente');
				Session::flash('class','success');
			}
		}

		return Redirect::route('clients.index');
		
	}

	/**
	 * Remove the specified usuario from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Cliente::destroy($id);
		Session::flash('message', 'El usuario fue borrado satisfactoriamente');
		Session::flash('class','success');
		return Redirect::route('clients.index');
	}

}
