<?php

class UsuariosController extends \BaseController {

	/**
	 * Display a listing of usuarios
	 *
	 * @return Response
	 */
	public function index()
	{
		$usuarios = Usuario::all();

		return View::make('backend.usuarios.index', compact('usuarios'));
	}

	/**
	 * Show the form for creating a new usuario
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.usuarios.create');
	}

	/**
	 * Store a newly created usuario in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$usuario=new Usuario;
		
		$validaciones = Usuario::validacionescreate(Input::all());

		if ($validaciones->fails())
		{
			return Redirect::back()->withErrors($validaciones)->withInput();
		}
		else
		{
					
			$usuario->nombre=Input::get('nombre');
			$usuario->login=Input::get('email');
			$usuario->email=Input::get('email');
			$usuario->rol=Input::get('rol');
			$usuario->clave=Hash::make(Input::get('clave'));

			if($usuario->save())
			{
				Session::flash('message','Guardado correctamente!');
				Session::flash('class','success');
			}
			else
			{
				Session::flash('message','Ha ocurrido un error');
				Session::flash('class','danger');
			}
		}

		return Redirect::route('users.index');
		
	}

	/**
	 * Display the specified usuario.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$usuario = Usuario::findOrFail($id);

		return View::make('backend.usuarios.show', compact('usuario'));
	}

	/**
	 * Show the form for editing the specified usuario.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$usuario = Usuario::find($id);

		return View::make('backend.usuarios.edit', compact('usuario'));
	}

	/**
	 * Update the specified usuario in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$usuario = Usuario::findOrFail($id);

		$datos = [
			'nombre' => Input::get('nombre'),
			'login' => Input::get('email'),
			'email' => Input::get('email'),
			'rol' => Input::get('rol'),
			'estatus' => Input::get('estatus'),
		];
		$rules = [
			'nombre' => 'required|min:5|max:30',
			'email' => 'required|email|min:5|max:30',
			'rol' => 'required',
			'estatus' => 'required',
		];
		
		$msj = [
			'nombre.required' => 'El campo <strong>Nombre</strong> esta vacio',
			'nombre.min' => 'El campo <strong>Nombre</strong> debe tener minimo :min caracteres',
			'nombre.max' => 'El campo <strong>Nombre</strong> debe tener maximo :max caracteres',
			'email.required' => 'El campo <strong>Correo electronico</strong> debe tener maximo :max caracteres',
			'email.required' => 'El campo <strong>Correo electronico</strong> es requerido',
		 	'email.email' => 'El formato del campo <strong>Correo electronico</strong> no es valido',
		 	'email.min' => 'El campo <strong>Correo electronico</strong> debe contener minimo :min carecteres',
		 	'email.max' => 'El campo <strong>Correo electronico</strong> debe contener maximo :max carecteres',
		 	'rol.required' => 'El campo <strong>Rol</strong> es reqerido',
		 	'estatus.required' => 'El campo <strong>Estatus</strong> es requerido',
		];

		$validator=Validator::make($datos,$rules,$msj);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		else
		{

			if (!empty(Input::get('clave'))) {
				$datos['clave'] = Hash::make(Input::get('clave'));
			}
		
			if($usuario->update($datos))
			{
				Session::flash('message','Actualizado Correctamente');
				Session::flash('class','success');
			}
		}

		return Redirect::route('users.index');
		
	}

	/**
	 * Remove the specified usuario from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Usuario::destroy($id);
		Session::flash('message', 'El usuario fue borrado satisfactoriamente');
		Session::flash('class','success');
		return Redirect::route('users.index');
	}

}
