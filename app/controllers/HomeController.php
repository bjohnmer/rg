<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		return View::make('frontend.landing');
	}

	public function login()
	{
		$validaciones = Usuario::validacioneslogin(Input::all());
 		
		if ($validaciones->fails())
		{
			return Redirect::to('/')->withErrors($validaciones)->withInput();
		}
		else
		{

			$email = Input::get('email');
			$clave = Input::get('clave');
			$usuario=Usuario::where('login','=', $email)->where('estatus','=', 'Activo')->get();
			
			if ( $usuario->count() == 1 )
			{
				
				echo $usuario[0]->clave;
				if(Hash::check($clave, $usuario[0]->clave))
				{
					Session::put('id',$usuario[0]->id);
					Session::put('nombre',$usuario[0]->nombre);
					Session::put('login',$usuario[0]->login );
					Session::put('email',$usuario[0]->email );
					Session::put('rol',$usuario[0]->rol );
					Session::put('estatus',$usuario[0]->estatus );
					Session::put('logged',TRUE);
					return Redirect::to('/dashboard');
				}
				else
				{
					Session::flash('message','La clave no es válida');
					Session::flash('class','danger');
				}		
			}
			else
			{
				Session::flash('message','El Usuario no está activo o registrado en el sistema');
				Session::flash('class','danger');
			}

			return Redirect::to('/');
		}
	}

	public function newreg()
	{
		return View::make('frontend.registrarse');
	}
		
	public function save()
	{
		$usuario=new Usuario;
		
		$validaciones = Usuario::validacionesinsertar(Input::all());

		if ($validaciones->fails())
		{
			return Redirect::to('/newreg')->withErrors($validaciones)->withInput();
		}
		else
		{
					
			$usuario->nombre=Input::get('nombre');
			$usuario->login=Input::get('email');
			$usuario->email=Input::get('email');
			$usuario->clave=Hash::make(Input::get('clave'));

			if($usuario->save())
			{
				Session::flash('message','Guardado correctamente!');
				Session::flash('class','success');
			}
			else
			{
				Session::flash('message','Ha ocurrido un error');
				Session::flash('class','danger');
			}
		}

		return Redirect::to('/newreg');
		
	}

	public function logout()
	{
		
		Session::flush();
		Session::forget('logged');
		Session::put('logged',FALSE);
		return Redirect::to('/');
	}

}