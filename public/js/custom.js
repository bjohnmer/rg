$(document).ready(function() {
  $('.eliminar').click(function(event) {
    if (!confirm('¿Desea Eliminar este registro?')) {
      return false;
    };
  });
  $('.logout').click(function(event) {
    if (!confirm('¿Desea Cerrar la Sesión del Usuario?')) {
      return false;
    };
  });
});